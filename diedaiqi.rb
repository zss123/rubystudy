class Book
  attr_accessor :title,:author

  def initialize(title,author)
    @title = title
    @author = author
  end
end

class BookList
  def initialize()
    @book_list = Array.new
  end

#添加
  def add(book)
    @book_list.push(book)
  end

# 长度
  def length
    @book_list.length
  end

# 获取哪一本书,数组中的元素,同一个类方法名不能同名
  def tt(n=1)
    @book_list[n]
  end

  # 替换哪一本书
  def [](n,book)
      @book_list[n] = book
  end

    #删除一本书
    def delete(book)
      @book_list.delete(book)
    end

    # 自定义一个迭代器
    def each_title
      @book_list.each { |book|
        # yield是定义自己的内容
          yield (book.title)
        }
    end
# 创建几本book，再加入到集合里面，调用each_title,
# BookList.new.each_title{ |title|
# puts title
# }
end
