class Box
  attr_accessor :w,:h

  def initialize(w,h)
    @w,@h = w,h
  end

# 面积
  def area
    @area = @w*@h
    puts "面积是#{@area}"
  end

# 周长
def length
  @length =@w+@h
  puts "周长是#{@length}"
end

# protect 方法
 def protectMethod
  puts"只能被本类和子类的对象调用"
end

# protected
 protected :protectMethod
 # Protected 方法： Protected 方法只能被类及其子类的对象调用。访问也只能在类及其子类内部进行。
 # ??????????????调用不了

# 私有,就不能获b.w   b.h。只能是类方法可以访问私有成员
private  :w,:h

end

# 子类继承
class BigBox < Box
  # 子类的方法
  def sonMethods
    puts"这是子类方法#{@area}"
  end
end
