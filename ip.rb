class Ip
  require 'json'
  require 'csv'
  require 'net/http'

  def initialize(file_name)
    @file_name = file_name
  end

  def get_ip_area
    arr = Array.new
    CSV.parse(File.read(@file_name)) do |row|
        uri =  URI.parse("http://apis.juhe.cn/ip/ip2addr?ip=#{row[0]}&key=82eac43b0877b836cc58d5697d080ee6")
        net = Net::HTTP.get_response(uri)
        json = JSON.parse(net.body)
        area = json['result']['area']
        ip_results = []
        ip_results.push(row[0].chomp)
        ip_results.push(area)
        arr.push(ip_results)
    end

    CSV.open('ip.csv', 'w') do |data|
      arr.each do |item|
        data << item
    end
  end
end
