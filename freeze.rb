class Hugo
attr_accessor :a,:b

def initialize(a,b)
  @a,@b = a,b
end

# 冻结
# def hugofreezd（c）
#   # 调用本身的对象冻结起来
# c.freeze
# puts"冻结"
# end

# 判断是否冻结
# def isFreezd(c)
#   if c.isFreezd?
#       puts "已经冻结"
#   else
#     puts"没有冻结"
#   end
# end

# h = Hugo.new(1,1)
#
# h.isFreezd(h)
# h.hugofreezd(h)
# h.isFreezd(h)

end
