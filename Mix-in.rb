module First
  A = 1
  def eat
    puts "eat something"
  end

# 模块自己的类方法,User不能调用,永远都不能被混入的
  def self.run
    puts "run something"
  end
end

module Second
  B = 2
  def say
    puts "sssss"
  end

  def self.look
    puts "books something"
  end
end

class User

  # Mix - in有两种 include  extend
  # include 通过实例调用
 include First
 # extend 可以通过类名掉用
 extend First
 # extend 能用类名调用，但是没有include 不能通过实例调用
 extend Second

  def initialize(no)
    @no = no
  end
end
