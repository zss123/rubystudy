# class Student
#   # 构造函数，实例化
#   def initialize(name,age)
#   @name = name
#   @age = age
#   end
#   # 方法
#   def sayHi
#     puts "我是#{@name} ,年龄#{@age}"
#   end
# end

# irb
# require ‘路径’
#  a = 类名.new(参数1，参数2)
# a.方法名
# exit 退出


# # 类名首字母大写！！！！！！
#   class Person
#   #  构造
#   def initialize(name,age,num)
#     @name = name
#     @age = age
#     @num = num
#   end
# # setter，=和（）之间不要有空格，空格就是java中的;
#   def name =(name)
#       @name = name
#   end
# # getter
#   def name
#     return @name
#   end
# # 普通方法
#   def play
#     puts "你好啊"
#   end
# end



# class Dog
#   # 类的常量第一个字母大写
#   Version = "1.0"
#   User = "hugo"
#
#   # set和get
#   attr_accessor :name
#
# # 只能get
#   attr_reader :age
#
# # 只能set
#   attr_writer :num
#
#  def initialize(name,age,num)
#    @name = name
#    @age = age
#    @num  = num
#  end
#
# #类方法
# def self.nick_name
#   puts "goubao"
# end
#
# def say
#   puts "wang wang"
# end
#
# end
# # 类常量的终端操作，  Dog::Version
# # 类方法直接用类名.调用方法
#
#
# # 继承
# class BigDog <Dog
#   def eat
#     puts "BigDog eat more"
#   end
#
# # 重写
#   def say
#     puts "wang  wang  wang"
#   end
# end
#
# # 权限 public private
#
# # 扩充类
# class Dog
#   # 这是一个实例的类方法
#   def run
#       puts "dog can run"
#   end
# end



#类方法，不需要只要原来的类是咋样的，相加就加
class String
    def self.toMake
      puts"类方法和实例的方法是不同的"
  end

  def self.give
    puts"give new skill "
  end
end
# 这里既可以用 String.toMake 也可以String::toMake在终端调用

# 
