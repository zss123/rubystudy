#1.不能实例化，例如Math
#2. 可以调用常量和方法  Math::PI 和 Math.sqrt(2)

module Ma

  PI = 3.14
  # 类方法
  def self.sqrt(number)
    Math.sqrt(number)
  end

# 实例方法也可以
  def help
    puts "实例方法也是可以的，Mix-in"
  end
end


class Student
  # 这样就有了M的能力
  include Ma

  def initialize(name)
    @name = name
  end
end

# Student::PI
# s = Studdent.new("sss")   s.help
# 模块的意义就是把有意义的方法和常量组成一个包，谁要就谁调用

# 条件运算符， c = (a>b) ? a : b 要有空格的
